use strict;
use warnings;
use Fcntl qw(SEEK_SET);
use Digest::MD5;
use Data::Dumper;
use Carp;

my %config = (
   memorydumpfile => {
      value     => "scriptfunctiontable.bin",
      arguments => "string",
      help      => "Name of the file containing the memory dump from no\$psx",
   },
   scriptdumpfile => {
      value     => "scriptfunctions.asm",
      arguments => "string",
      help      => "Name of the file containing the assembly dump from no\$psx",
   },
   printtypes    => {
      value     => "all",
      arguments => "csv",
      help      => "Comma separated value to show only a subset of message types",
   },
   printarithmetic => {
      value     => 1,
      arguments => "boolean",
      help      => "Show arithmetical operations.",
   },
   printjumps => {
      value     => 1,
      arguments => "boolean",
      help      => "Show jump operations.",
   },
   printcalls => {
      value     => 1,
      arguments => "boolean",
      help      => "Show call operations.",
   },
   printmoves => {
      value     => 1,
      arguments => "boolean",
      help      => "Print any type of moves.",
   },
   printmemmoves => {
      value     => 1,
      arguments => "boolean",
      help      => "Show moves in and out of memory.",
   },
   printregmoves => {
      value     => 1,
      arguments => "boolean",
      help      => "Show moves from register to register.",
   },
   showcalltree => {
      value     => 0,
      arguments => "toggle",
      help      => "Show call trees",
   },
   debug => {
      value     => 0,
      arguments => "toggle",
      help      => "Turns on debug messages.",
   },
);

my %tool = (
   name          => "scriptfunction.pl",
   description   => "This tool is used to parse through the xenogears script engine\n" .
                    "algorithms. This could potentially be used to help decode the\n" .
                    "scripts into a human readable format and make sure to only\n" .
                    "consume the appropriate number of values for the script to work.",
   standard_print => sub {
      print shift( @_ );
   },
   opcode_print  => sub {
      my ( $string, $condition ) = @_;

      print $string if ( $condition eq "printalways" );
      print $string if ( $config{$condition}{value} );
      print $string if ( $config{"print$condition"}{value} );
   },
   debug_print   => sub {
      print "scriptfunction.pl -D- " . shift( @_ ) . "\n" if( $config{debug}{value} );
   },
   info_print    => sub {
      print "scriptfunction.pl -I- " . shift( @_ ) . "\n";
   },
   warning_print => sub {
      print "scriptfunction.pl -W- " . shift( @_ ) . "\n";
   },
   error_print   => sub {
      croak "scriptfunction.pl -E- " . shift( @_ ) . "\n";
   },
   fatal_print   => sub {
      confess "scriptfunction.pl -F- " . shift( @_ ) . "\n";
   },
);

my %script_opcode = (
   "0000" => "Return()",
   "0001" => "Jump(U16 address)",
   "0002" => "ConditionalJump(U8 offset, U8 databank, U16 value, U8 condition, U16 address)",
   "0008" => "EntityCallScriptSW(E entity, U4 priority, U4 script)",
   "0009" => "EntityCallScriptEW(E entity, U4 priority, U4 script)",
   "000B" => "EntityNPCInit(V80 sprite_id)",
   "0016" => "EntityPCInit(V80 character_id)",
   "0019" => "SpriteSetPosition(VF80 x, VF40 y, F unknown)",
   "0020" => "SpriteSetSolid(VF80 type)",
   "0023" => "SetFlag0020()",
   "0026" => "Wait(V80 time)",
   "0029" => "EntityTurnOff(EV entity)",
   "002A" => "SetFlag0002()",
   "002C" => "SpritePlayAnimation(U8 animation_id)",
   "0036" => "VariableSetTrue(U8 offset, U8 databank)",
   "0037" => "VariableSetFalse(U8 offset, U8 databank)",
   "004A" => "SpriteGoToPosition(U16 x, U16 y, C0)",
   "0069" => "SpriteSetDirection(V80 direction)",
   "006B" => "SpriteRotateClockwise(V80 rotation)",
   "006C" => "SpriteRotateAntiClockwise(V80 rotation)",
   "006F" => "SpriteRotateToEntity(EV entity_id)",
   "0086" => "ProgressNotEqualJumpTo(V80 value)",
   "0091" => "CheckIfCharacterInParty(U8 character)",
   "0098" => "MapLoad(U16 map_id, V80 location)",
   "00A8" => "VariableRandom(U16 , V80 max_value)",
   "00A9" => "SetUserInputOptions(U8 offset_numterms)",
   "00B3" => "FadeOut(V80 time)",
   "00B4" => "FadeIn(V80 time)",
   "00B5" => "CameraSetDirection(V80 direction, V80 unknown)",
   "00B9" => "CheckPartyMemberExists(U8 character)",
   "00BA" => "SetPartyMember(U8 character)",
   "00BB" => "ClearPartyMember(U8 character)",
   "00BC" => "EntityNoModelInit()",
   "00C4" => "DoorOpen(U8 clockwise_b)",
   "00C5" => "DoorClose(U8 clockwise_b)",
   "00C7" => "CameraRotate(V80 time)",
   "00C8" => "CameraRotate2(V80 time)",
   "00D2" => "DialogShow(U16 dialog_id, U8 flag)",
   "FE0D" => "SetAvatar(V80 character_id)",
   "FE18" => "AddCharacterToParty(U8 character_id)",
);

my %offset_info = (
   "800ACDB8" => {
      type        => "function",
      name        => "getUInt16Value",
      description => "Used to get a 16-bit unsigned value from the script file.",
      inputs      => {
         r4 => "ArgumentOffset",
      },
      outputs     => {
         r2 => "ScriptArgument",
      },
   },

   "800ACD7C" => {
      type        => "function",
      name        => "getSInt16Value",
      description => "Used to get a 16-bit Signed value from the script file.",
      inputs      => {
         r4 => "ArgumentOffset"
      },
      outputs     => {
         r2 => "ScriptArgument",
      },
   },

   "80072254" => {
      type        => "function",
      name        => "80072254",
      description => "Usage is so far unknown but does appear to be using some serious arithmetic operations",
      inputs      => {
         r4 => "EntityNumber",
      },
   },
   "8003F738" => {
      type        => "function",
      name        => "8003F738",
      description => "Usage is unknown but does a lot of multiplications",
      inputs      => {
         r4 => "IndexedEntityStructPtr+50",
         r5 => "IndexedEntityStructPtr+C",
      },
   },
   "8003FA38" => {
      type        => "function",
      name        => "8003FA38",
      description => "Modifies memory [8005A1FC]=(([8005A1FC]*41C64E6D)+3039)",
      outputs     => {
         r1 => "80060000",
         r2 => "(((([8005A1FC]*41C64E6D)+3039)>>10)&7FFF)",
         r3 => "41C64E6D",
         r4 => "[8005A1FC]*41C64E6D",
      },
   },
   "80049DCC" => {
      type        => "function",
      name        => "80049DCC",
      description => "Usage is unknown but does a lot of multiplications",
      inputs      => {
         r4 => "IndexedEntityStructPtr",
         r5 => "[[IndexedEntityStructPtr+4C]+F4]",
      },
   },

   "8009C5A8" => {
      type        => "function",
      name        => "DialogFunction",
      description => "Appears to play a role in dialog boxes. r5=1=DialogShow",
      inputs      => {
         r4 => "EntityIndex",
         r5 => "DialogFunctionType",
      },
   },

   "800ACDEC" => {
      type        => "function",
      name        => "getV80Variable",
      description => "Used to call getUInt16Value if 0x8000 masked then returns (original value & 0x7FFF) else will use get16Variable with proper signed types from memory.",
      inputs      => {
         r4 => "ArgumentOffset",
      },
      outputs     => {
         r2 => "ScriptArgument",
         r4 => "VariableValue",
      },
   },
   
   "8009CF78" => {
      type        => "function",
      name        => "getVF80Variable",
      description => "Used to go fetch a 16-bit value from the script file. If bit7 is set then the value will be read as a signed value straight from the script file. Otherwise, it will be signed/unsigned depending on the script file header and read out as a 16-bit variable based on the address pulled from the unsigned read from the script file.",
      inputs      => {
         r4 => "ArgumentOffset",
         r5 => "FVariable",
      },
      outputs     => {
         r4 => "ScriptArgument",
      },
   },
   
   "8009CFBC" => {
      type        => "function",
      name        => "getVF40Variable",
      description => "Used to go fetch a 16-bit value from the script file. If bit6 is set in the r5 register then read a signed value from the script file. Otherwise, go get the variable based on the unsigned 16-bit value from the script file. The signed/unsigned integer type will be determined by the script files header.",
      inputs      => {
         r4 => "ArgumentOffset",
         r5 => "FVariable",
      },
      outputs     => {
         r4 => "ScriptArgument",
      },
   },
   "8009D000" => {
      type        => "function",
      name        => "getVF20Variable",
      description => "Used to go fetch a 16-bit value from the script file. If bit5 is set then the value will be read as a signed value straight from the script. Otherwise, it will be signed/unsigned depending on the script file header and read out as a 16-bit variable based on the address pulled from the unsigned read from the script file.",
      inputs      => {
         r4 => "ArgumentOffset",
         r5 => "FVariable",
      },
      outputs     => {
         r4 => "ScriptArgument",
      },
   },
   "8009D044" => {
      type        => "function",
      name        => "getVF10Variable",
      description => "Used to go fetch a 16-bit value from the script file. If bit4 is set then the value will be read as a signed value straight from the script. Otherwise, it will be signed/unsigned depending on the script file header and read out as a 16-bit variable based on the address pulled from the unsigned read from the script file.",
      inputs      => {
         r4 => "ArgumentOffset",
         r5 => "FVariable",
      },
      outputs     => {
         r4 => "ScriptArgument",
      },
   },
   "8009D088" => {
      type        => "function",
      name        => "getVF08Variable",
      description => "Used to go fetch a 16-bit value from the script file. If bit3 is set then the value will be read as a signed value straight from the script. Otherwise, it will be signed/unsigned depending on the script file header and read out as a 16-bit variable based on the address pulled from the unsigned read from the script file.",
      inputs      => {
         r4 => "ArgumentOffset",
         r5 => "FVariable",
      },
      outputs     => {
         r4 => "ScriptArgument",
      },
   },
   "8009D0CC" => {
      type        => "function",
      name        => "getVF04Variable",
      description => "Used to go fetch a 16-bit value from the script file. If bit2 is set then the value will be read as a signed value straight from the script. Otherwise, it will be signed/unsigned depending on the script file header and read out as a 16-bit variable based on the address pulled from the unsigned read from the script file.",
      inputs      => {
         r4 => "ArgumentOffset",
         r5 => "FVariable",
      },
      outputs     => {
         r4 => "ScriptArgument",
      },
   },
   "8009D110" => {
      type        => "function",
      name        => "getVF02Variable",
      description => "Used to go fetch a 16-bit value from the script file. If bit1 is set then the value will be read as a signed value straight from the script. Otherwise, it will be signed/unsigned depending on the script file header and read out as a 16-bit variable based on the address pulled from the unsigned read from the script file.",
      inputs      => {
         r4 => "ArgumentOffset",
         r5 => "FVariable",
      },
      outputs     => {
         r4 => "ScriptArgument",
      },
   },
   "8009D154" => {
      type        => "function",
      name        => "getVF01Variable",
      description => "Used to go fetch a 16-bit value from the script file. If bit0 is set then the value will be read as a signed value straight from the script. Otherwise, it will be signed/unsigned depending on the script file header and read out as a 16-bit variable based on the address pulled from the unsigned read from the script file.",
      inputs      => {
         r4 => "ArgumentOffset",
         r5 => "FVariable",
      },
      outputs     => {
         r4 => "ScriptArgument",
      },
   },
   "8009CDB4" => {
      type        => "function",
      name        => "getEVariable",
      description => "This function will get an unsigned 8-bit value from memory. If the entity number is the special case of 0xFF, 0xFE, 0xFD or 0xFB then it will grab from a unique table the 'true' values and send them out instead.",
      inputs      => {
         r4 => "ArgumentOffset",
      },
      outputs     => {
         r2 => "ScriptArgument",
      },
   },
   "800A3018" => {
      type        => "function",
      name        => "get16Variable",
      description => "Gets a 16-bit variable that is either signed or unsigned depending on the bit flags defined at the top of the script file. It appears that r3 being 0 means that the final value is treated as signed. Therefore a value of 1 is unsigned.",
      inputs      => {
         r4 => "VariableAddress",
      },
      outputs     => {
         r1 => "VariableAddressPtr",
         r2 => "VariableValue",
         r3 => "VariableUnsignedIndicator",
         r4 => "VariableAddress>>1&1F",
         r5 => "VariableAddress>>1",
      },
   },
   "800A3074" => {
      type        => "function",
      name        => "setU16Variable",
      description => "Used to set a 16-Bit variable from the global script memory.",
      inputs      => {
         r4 => "VariableAddress",
         r5 => "VariableValue",
      },
   },
   "800A3090" => {
      type        => "function",
      name        => "getEntityScriptOffset",
      description => "Used to get the offset value pointed to by Entity(r4) Script(r5) in the script entity table",
      inputs      => {
         r4 => "EntityNumber",
         r5 => "ScriptNumber",
      },
      outputs     => {
         r2 => "ScriptOffset",
      },
   },
   "8004A70C" => {
      type        => "function",
      name        => "getNormalClipping",
      description => "Makes calls to the Geometry Transformation Engine for normal clipping. This may be to detect collitions. Not sure why the script entine requires this yet.",
      inputs      => {
         r4 => "SXY0",
         r5 => "SXY1",
         r6 => "SXY2",
      },
      outputs     => {
         r2 => "MAC0",
      },
   },
   "80085634" => {
      type        => "function",
      name        => "PlaySoundFunction",
      description => "Function called to play sounds",
   },
   "8008F7B8" => {
      type        => "function",
      name        => "PlayMusicFunction",
      description => "Function called to play music",
   },
   "8008A790" => {
      type        => "function",
      name        => "CheckCharacterInActiveParty",
      description => "Checks if the current character is in one of the three active party member slots.",
      inputs      => {
         r4 => "CharacterID",
      },
      outputs     => {
         r2 => "CharacterExists",
         r3 => "CharacterSlotIndex",
         r6 => "CharacterSlotAddress",
         r7 => "FF",
      }
   },
   "8008A7DC" => {
      type        => "function",
      name        => "8008A7DC",
      description => "Sets up variables to let other functions know that a character is being added to the main party.",
   },
   "8008A558" => {
      type        => "function",
      name        => "8008A558",
      description => "Selects a subfunction based on memory conditions",
      inputs      => {
         r4 => "select",
      },
   },
   "80028A60" => {
      type        => "function",
      name        => "80028A60",
      description => "Selects a function based on the input r4",
      inputs      => {
         r2 => "LoopValue",
         r4 => "ExecuteOnce",
      },
   },
   "800286CC" => {
      type        => "function",
      name        => "800286CC",
      description => "If [8004FDFC]!=0 {
                        r2=[8004FDFC]
                      } elsif [8004FE48]!=0 {
                        r4=1
                        if [8004FE1C]!=0 {
                           r2=1
                        } else {
                           r2=[8004FDFC]
                        }
                      } else {
                         call 80041410
                         if [8004FE48]==0 {
                            if [8004FE1C]!=0 {
                               r2=1
                            } else {
                               r2=[8004FDFC]
                            }
                         }
                      }",
   },
   "80098CAC" => {
      type        => "function",
      name        => "80098CAC",
      description => "Used by function 0010 and 0011. Usage varies, appears to be battle ring or debug related",
      inputs      => {
         r4 => "FunctionType",
      },
   },
   "[800B2176]" => {
      type        => "memory",
      name        => "DisablePlayerMovement",
      description => "Player control is disabled when this contains a non-zero value",
   },
   "[800B21D0]" => {
      type        => "memory",
      name        => "800B21D0",
      description => "Joins with disabling the player and compass, not sure it has a function itself",
   },
   "[800B21D1]" => {
      type        => "memory",
      name        => "DisableCompass",
      description => "On screen compass is disabled by writing a 1 to this value",
   },
   "[800ADB2C]" => {
      type        => "memory",
      name        => "MusicPlaying",
      description => "May be an indicator that a specific channel is playing a song. Gets set in only one spot after the PlayMusic function finished. More searching may uncover that when we want to do something with music or sounds that this may get checked for.",
   },
   "[800ADBF8]" => {
      type        => "memory",
      name        => "ScriptHeader",
      description => "Script header or insertion point in memory",
   },
   "800AE6A0" => {
      type        => "memory",
      name        => "ExtendedOpcodeFunctionTable",
      description => "Table that contains all the extended opcodes.",
   },
   "[800ADBC4]" => {
      type        => "memory",
      name        => "PartyModifierEventNotice",
      description => "When set to FF indicates the party is not being modified.",
   },
   "[800B0078]" => {
      type        => "memory",
      name        => "ScriptStruct",
      description => "Data structure that seems to contain information about the current levels script.",
   },
   "ScriptStruct+CC" => {
      type        => "memory",
      name        => "ScriptCodeIndexPtr",
      description => "Pointer to the Scripts Current Code Index",
   },
   "[ScriptCodeIndexPtr]" => {
      type        => "memory",
      name        => "ScriptCodeIndex",
      description => "The scripts current code index",
   },
   "ScriptCodeIndex+ScriptCodeBlock" => {
      type        => "memory",
      name        => "AbsoluteScriptCodeIndex",
      description => "Points to the actual location in memory where the argument offsets exists",
   },
   "8006D8E4" => {
      type        => "memory",
      name        => "FeiDataStructure",
      description => "Fei Next Level",
   },
   "8006E00C" => {
      type        => "memory",
      name        => "WeltallDataStructure",
      description => "If we have faith in the gameshark codes this is where Weltal1 data sits",
   },
   "[8004F34C]" => {
      type        => "memory",
      name        => "MapID",
      description => "Used as the index for the next level to be loaded.",
   },
   "[8005A444]" => {
      type        => "memory",
      name        => "EntityPlayer",
      description => "Appears to be the pointer to the player entity.",
   },
   "[800AFD1C]" => {
      type        => "memory",
      name        => "EntityCurrent",
      description => "Appears to be a pointer to the current entity the script is working with.",
   },
   "[800AFB10]" => {
      type        => "memory",
      name        => "EntityStructArray",
      description => "Pointer to the Entity Structure Array",
   },
   "[[8005A39C]+1D30]" => {
      type        => "memory",
      name        => "PartyMembers",
      description => "Party members vector active part members if the following bits are set: 0=Fei, 1=Elly, 2=Citan, 3=Bart, 4=Billy, 5=Rico, 6=Emerald, 7=ChuChu, 8=Maria, 9=CitanSword, 10=AdultEmerald",
   },
   "[80062590]" => {
      type        => "memory",
      name        => "PartyCharacterSlot1",
      description => "Character party member slot number 1. Contains the value of the current character",
   },
   "[80062594]" => {
      type        => "memory",
      name        => "PartyCharacterSlot2",
      description => "Character party member slot number 2. Contains the value of the current character",
   },
   "[80062598]" => {
      type        => "memory",
      name        => "PartyCharacterSlot3",
      description => "Character party member slot number 3. Contains the value of the current character",
   },
   "EntityStructArray+EntityPlayer<<1+EntityPlayer<<3-EntityPlayer<<2" => {
      type        => "memory",
      name        => "PlayerEntityStructPtr",
      description => "Pointer to the Current Entity Structure",
   },
   "EntityPlayer<<1+EntityPlayer<<3-EntityPlayer<<2+EntityStructArray" => {
      type        => "memory",
      name        => "PlayerEntityStructPtr",
      description => "Pointer to the Current Entity Structure",
   },
   "EntityStructArray+EntityCurrent<<1+EntityCurrent<<3-EntityCurrent<<2" => {
      type        => "memory",
      name        => "CurrentEntityStructPtr",
      description => "Pointer to the Current Entity Structure",
   },
   "EntityCurrent<<1+EntityCurrent<<3-EntityCurrent<<2+EntityStructArray" => {
      type        => "memory",
      name        => "CurrentEntityStructPtr",
      description => "Pointer to the Current Entity Structure",
   },
   "EntityStructArray+ScriptArgument<<1+ScriptArgument<<3-ScriptArgument<<2" => {
      type        => "memory",
      name        => "IndexedEntityStructPtr",
      description => "Pointer to an indexed EntityStructPtr value",
   },
   "ScriptArgument<<1+ScriptArgument<<3-ScriptArgument<<2+EntityStructArray" => {
      type        => "memory",
      name        => "IndexedEntityStructPtr",
      description => "Pointer to an indexed EntityStructPtr value",
   },
   "EntityStructArray+EntityNumber<<1+EntityNumber<<3-EntityNumber<<2" => {
      type        => "memory",
      name        => "IndexedEntityStructPtr",
      description => "Pointer to an indexed EntityStructPtr value",
   },
   "EntityNumber<<1+EntityNumber<<3-EntityNumber<<2+EntityStructArray" => {
      type        => "memory",
      name        => "IndexedEntityStructPtr",
      description => "Pointer to an indexed EntityStructPtr value",
   },
   "ScriptHeader+EntityNumber<<5+ScriptNumber<<1+84" => {
      type        => "memory",
      name        => "EntityScriptOffsetPtr",
      description => "Indexed entity script offset pointer in memory",
   },
   "[EntityScriptOffsetPtr]" => {
      type        => "memory",
      name        => "EntityScriptOffset",
      description => "For a given entity index and script index gets from the script where within the script the offset can be found.",
   },
   "[PlayerEntityStructPtr]" => {
      type        => "memory",
      name        => "PlayerEntityStruct",
      description => "Player Entity Structure",
   },
   "[CurrentEntityStructPtr]" => {
      type        => "memory",
      name        => "CurrentEntityStruct",
      description => "Current Entity Structure",
   },
   "[800B0144]" => {
      type        => "memory",
      name        => "ScriptCodeOffset",
      description => "Points to the location that the script file has been dumped into."
   },
   "[800ADC00]" => {
      type        => "memory",
      name        => "ScriptCodeBlock",
      description => "ScriptCodeBlock + ScriptCodeIndex should give the exact location in memory where the current script is running.",
   },
   "800C0000" => {
      type        => "memory",
      name        => "BaseMemoryAddress",
      description => "Not known but most of the major data structures are based around this address. This could be the memory space that was assigned by the compiler. There are elements both + and - this region.",
   },
   "[800C3A68]" => {
      type        => "memory",
      name        => "GlobalMemory",
      description => "Not clear yet if this is global memory for maps to talk to each other, or if it is just for the current level scripts",
   },
);

command_line_parser( 1, 1 );

if( $config{printtypes}{changed} ) {
   $config{printarithmetic}{value} = 0;
   $config{printjumps}{value}      = 0;
   $config{printcalls}{value}      = 0;
   $config{printmemmoves}{value}   = 0;
   $config{printregmoves}{value}   = 0;
   $config{printmoves}{value}      = 0;

   foreach my $value ( split( /,/, $config{printtypes}{value} ) ) {
      $tool{debug_print}(
         sprintf( "Print Type key: %s",
            $value
         )
      );
      if( $value eq "all" ) {
         $config{printarithmetic}{value} = 1;
         $config{printjumps}{value}      = 1;
         $config{printcalls}{value}      = 1;
         $config{printmemmoves}{value}   = 1;
         $config{printregmoves}{value}   = 1;
         $config{printmoves}{value}      = 1;
      } elsif( $value =~ m/(.*)/ ) {
         if( exists( $config{$1} ) ) {
            $config{$1}{value} = 1;
         } elsif( exists( $config{"print$1"} ) ) {
            $config{"print$1"}{value} = 1;
         } else {
            $tool{error_print}(
               sprintf( "Command line switch printtypes was loaded with %s which is not a valid print type\n",
                  $1
               )
            );
         }
      }
   }
}

my %calls; # Shallow version of the call stack, no more than 1 deep
my %offset_table      = %{ getOpcodeOffsetTable( ) };
my %file_offset       = %{ parseScriptDumpFile( ) };
my %subfunction_table = %{ dumpOpcodeAssembly( ) };
my %found_calls;
dumpExtraFunctions( \%subfunction_table, \%found_calls );
foreach my $found_call ( sort( { $found_calls{$b} <=> $found_calls{$a} } keys( %found_calls ) ) ) {
   my $called = $found_calls{$found_call};
   
   if( exists( $offset_info{$found_call} ) ) {
      $found_call = sprintf( "%s (%s)", $offset_info{$found_call}{name}, $found_call );
   }
   $tool{standard_print}(sprintf( "Subfunction %s was called %d times\n", $found_call, $called ));
}
   
if( $config{showcalltree}{value} ) {
   showcalltreefunc( );
}

exit 0;

sub showcalltreefunc {
   $tool{standard_print}( "\n\nCall Tree View:\n\n" );
   foreach my $cmd_key ( sort( { $a <=> $b } keys( %offset_table ) ) ) {
      my $region_start  = $offset_table{$cmd_key}{offset};
      my @family = ();
      calltrace( $region_start, "", 0, 0, \@family );
   }
   $tool{standard_print}( "\n\n" );
}

sub calltrace {
# value1
# `+- subvalue1
#  +- subvalue2
#  `+- subsubvalue1
#   |`+- subsubsubvalue1
#   | +- subsubsubvalue2
#   +- subsubvalue2 
   my $current  = shift( @_ );
   my $header   = shift( @_ );
   my $sibling  = shift( @_ );
   my $first    = shift( @_ );
   my @family   = @{ shift( @_ ) }; #only care what came before so use copy function
   my $current_name = $current;

   if( exists( $offset_info{$current} ) ) {
      $current_name = sprintf( "%s (%s)",
                               $offset_info{$current}{name},
                               $current_name
                             );
   }

   my $local_header = $header;

   if( $first ) {
      $local_header .= "`";
      $header       .= " ";
   } else {
      $local_header .= " ";
      $header       .= " ";
   }
   if( $sibling ) {
      $local_header .= "+- ";
      $header       .= "|  ";
   } else {
      $local_header .= "-- ";
      $header       .= "   ";
   }

   $tool{standard_print}(
      sprintf( "%s%s\n",
         $local_header,
         $current_name
      )
   );

   foreach my $parent ( @family ) {
      if( $current eq $parent ) {
         return;
      }
   }

   push( @family, $current );

   if( exists( $calls{$current} ) ) {
      my $count     = keys( %{ $calls{$current} } );
      my $cur       = 1;
      my $first     = 1;

      foreach my $subcall ( keys( %{ $calls{$current} } ) ) {
         calltrace( $subcall, $header, $count-$cur, $first, \@family );
         $first = 0;
         $cur  += 1;
      }
   }
}

sub command_line_parser {
   my $terminate_on_errors = shift( @_ );
   my $terminate_on_help   = shift( @_ );

   my $argument = "";
   my $switch   = "";
   my $switches_have_errors = 0;

   foreach my $arg ( @ARGV ) {
      if( $switch ne "" ) {
         $config{$switch}{value}   = $arg;
         $config{$switch}{changed} = 1;
         $tool{debug_print}(
            sprintf( "set %s to %s",
               $switch,
               $config{$switch}{value}
            )
         );
         $switch                   = "";
         $argument                 = "";
      } elsif( $arg =~ m/-(\w+)/i ) {
         $switch = $1;
         
         if( $switch eq "help" ) {
            print "Usage:       $tool{name} [-<switch> [<argument]]\n";
            print "Description: $tool{description}\n";
            print "=" x 80 . "\n";
            printf( "%-20s %-20s %-40s\n", "Switch", "Argument Type", "Current Value" );
            print "-" x 80 . "\n";
            foreach my $configval ( keys( %config ) ) {
               printf( "%-20s %-20s %-40s\n", $configval, $config{$configval}{arguments}, $config{$configval}{value} );
               print "Description:\n";
               print "$config{$configval}{help}\n---\n";
            }
            exit 0;
         } elsif( exists( $config{$switch} ) ) {
            $argument = $config{$switch}{arguments};
            if( $argument eq "toggle" ) {
               if( $config{$switch}{value} ) {
                  $config{$switch}{value} = 0;
               } else {
                  $config{$switch}{value} = 1;
               }
               $tool{debug_print}(
                  sprintf( "toggled %s to %d",
                     $switch,
                     $config{$switch}{value}
                  )
               );
               $switch   = "";
               $argument = "";
            } elsif( $argument eq "" ) {
               $tool{error_print}( "$config{$switch}{name} is configured incorrectly" );
               $switch               = "";
               $switches_have_errors = 1;
            }
         } else {
            $tool{error_print}( "Unknown switch -$arg" );
         }
      }
   }

   foreach my $switch ( keys( %config ) ) {
      my $required = 0;
      my $updated = 0;

      $required = $config{$switch}{required} if( defined( $config{$switch}{required} ) );
      $updated = $config{$switch}{changed} if( defined( $config{$switch}{changed} ) );
      
      if( $required and !$updated ) {
         $switches_have_errors = 1;
         $tool{error_print}( "$config{$switch}{name} is required." );
      }
   }
   if( $switches_have_errors and $terminate_on_errors ) {
      croak "ERROR: Switches have errors\n";
   }
}

sub getOpcodeOffsetTable {
   my %offset_table;
   
   my $cmd = 0x00000000;
   my $table_offset = 0x800AE2A0;
   my $cmd_ptr = $table_offset + (($cmd & 0x000FF) << 0x02);
   my $opcodename = "UnknownOpcode0x";

   open( my $sft, '<', $config{memorydumpfile}{value} ) or croak "$!";

   while( my $line = <$sft> ) {
      if( $line =~ m/([0-9A-F]{8})\s((?:[0-9A-F]{2}\s){16})/i ) {
         my $offset = $1;
         my $data_array = $2;
         my $value = "";

         if( (hex($offset) == $cmd_ptr) and ($cmd < 0x0FF00) ) {
            foreach my $data (split( /\s/, $data_array )) {
               $value = $data . $value;
               
               if( length( $value ) == 8 ) {
                  $offset_table{$cmd}{offset} = $value;
                  # Only add a function if is is at least in the valid code region.
                  if( hex($value) >= 0x80000000 and hex($value) < 0x90000000 ) {
                     $offset_info{$value}{type} = "opcode";
                     $offset_info{$value}{name} = sprintf( "%04X", $cmd );
                     $offset_info{$value}{description} = "Automatically added from opcode tables";
                     if( exists( $script_opcode{$offset_info{$value}{name}} ) ) {
                        $offset_info{$value}{name} = $script_opcode{$offset_info{$value}{name}};
                     } else {
                        $offset_info{$value}{name} = $opcodename . $offset_info{$value}{name};
                     }
                  }
                  $cmd = $cmd + 1;
                  if( $cmd == 0x0100 ) {
                     $cmd = 0x0FE00;
                     $table_offset = 0x800AE6A0;
                     $opcodename   = "UnknownExtendedOpcode0x";
                  }
                  $cmd_ptr = $table_offset + (($cmd & 0x00FF) << 0x02 );
                  $value = "";
               }
            }
         }
      }
   }

   close( $sft );
   
   return \%offset_table;
}

sub parseScriptDumpFile {
   my %file_offset;
   
   open( my $sdt, "<", $config{scriptdumpfile}{value} ) or croak "$!";
   
   my $current_position = 0;
   my $last_offset      = "";
   
   while( my $line = <$sdt> ) {
      if( $line =~ m/([0-9A-F]{8})\s([0-9A-F]{8})\s(.+?)\r?\n?$/i ) {
         my $offset = $1;
         $line =~ s/\r?\n?//g;
         
         $file_offset{$offset}{pos}       = $current_position;
         $file_offset{$offset}{line}      = $line;
         $file_offset{$offset}{next}      = "";
         $file_offset{$last_offset}{next} = $offset if( $last_offset ne "" );
         $current_position                = tell( $sdt );
         $last_offset                     = $offset;
      }
   }
   
   close( $sdt );
   
   return \%file_offset;
}

sub dumpExtraFunctions {
   my $lookup_calls     = shift( @_ ); # List of calls that were made previously. 
   my $found_calls      = shift( @_ ); # List of calls that were actually looked up (sub function call to the current call).
   my %extra_calls      = ( );         # List of calls that need to be looked up (sub function call to the current call).
   
   foreach my $function_offset ( sort( { $lookup_calls->{$b} <=> $lookup_calls->{$a} } keys( %$lookup_calls ) ) ) {
      my $digest = Digest::MD5->new;
      my $found          = 0;
      my $region_entered = 0;
      my $offset         = "";
      
      # Store off a copy of the functions we are currently looking up to the
      # completed list of found calls. Put it there even if it didn't exist,
      # else you will keep looking up the same unfound function.
      $found_calls->{$function_offset} = $lookup_calls->{$function_offset};
      
      my $print_function_name = $function_offset;

      if( exists( $offset_info{$function_offset} ) ) {
         $print_function_name = sprintf( "%s (%s)",
                                    $offset_info{$function_offset}{name},
                                    $print_function_name
                                );
      }

      if( exists( $file_offset{$function_offset} ) ) {
         $offset = $function_offset;
      } else {
         $tool{info_print}(
            sprintf( "Could not find %s in the document for command %s\n",
               $function_offset,
               $print_function_name
            )
         );
         next;
      }

      my %script_hash = %{ getFullFunction( $offset ) };

      my %last_reg = (
         hi => "?",
         lo => "?",
         ra => "?",
         sp => "FFFFFFFF",
      );
      
      foreach my $regnum (1 ... 31) {
         $last_reg{"r$regnum"} = "?";
      }

      if( exists( $offset_info{$function_offset} ) ) {
         if( exists( $offset_info{$function_offset}{inputs} ) ) {
            foreach my $input ( keys( %{ $offset_info{$function_offset}{inputs} } ) ) {
               $last_reg{$input} = $offset_info{$function_offset}{inputs}{$input};
               $tool{standard_print}(
                  sprintf( "input(%s) = %s\n",
                     $input,
                     $last_reg{$input}
                  )
               );
            }
         }
      }

      $tool{standard_print}(
         sprintf( "%s for sub function %s:\n",
            $offset,
            $print_function_name
         )
      );
      

      foreach my $linenum ( sort( { $a <=> $b } keys( %script_hash ) ) ) {
         my %sub_calls;

         # Decode the current command line
         decodeScriptCommand( \%last_reg, \%sub_calls, $digest, $script_hash{$linenum} );

         # foreach sub function call made, add its count where appropriate
         foreach my $sub_call_function ( keys( %sub_calls ) ) {
            my $lookup_exists = exists( $lookup_calls->{$sub_call_function} );
            my $found_exists  = exists( $found_calls->{$sub_call_function} );
            my $extra_exists  = exists( $extra_calls{$sub_call_function} );
            my $sub_count     = $sub_calls{$sub_call_function};
            
            # Was already found, just add to the number of calls received
            if( $found_exists ) {
               $found_calls->{$sub_call_function} = $found_calls->{$sub_call_function} + $sub_count;
            }
            # Was already in the original lookup list just hasn't been moved to found yet.
            if( $lookup_exists ) {
               $lookup_calls->{$sub_call_function} = $lookup_calls->{$sub_call_function} + $sub_count;
            }

            # If it was part of the extra_calls list that was already lookedup
            if( $extra_exists ) {
               $extra_calls{$sub_call_function} = $extra_calls{$sub_call_function} + $sub_count;
            } elsif( $found_exists == 0 and $lookup_exists == 0 ) {
               # If it was not yet added to any list, it needs to be looked up later
               $extra_calls{$sub_call_function} = $sub_count;
            }

            $calls{$function_offset}{$sub_call_function} += 1;
         }
      }

      if( exists( $offset_info{$function_offset} ) ) {
         if( exists( $offset_info{$function_offset}{outputs} ) ) {
            foreach my $output ( keys( %{ $offset_info{$function_offset}{outputs} } ) ) {
               $last_reg{$output} = $offset_info{$function_offset}{outputs}{$output};
               $tool{standard_print}(
                  sprintf( "output(%s) = %s\n",
                     $output,
                     $last_reg{$output}
                  )
               );
            }
         }
      }
      
      $tool{standard_print}("\n\n");
   }
   
   # If extra calls contains functions that need to be looked up add it.
   if( scalar( keys( %extra_calls ) ) > 0 ) {
      # foreach extra call if it was already something lookedup remove it
      foreach my $extra_call ( keys( %extra_calls ) ) {
         if( exists( $found_calls->{$extra_call} ) ) {
            delete( $extra_calls{$extra_call} );
         }
      }
      
      # If anything still remains in the extra calls list, go ahead and recursively call
      if( scalar( keys( %extra_calls ) ) > 0 ) {
         dumpExtraFunctions( \%extra_calls, $found_calls );
      }
   }
}

sub dumpOpcodeAssembly {
   my @argument_handler = ();
   my %sub_calls;

   foreach my $cmd_key ( sort( { $a <=> $b } keys( %offset_table ) ) ) {
      my $digest        = Digest::MD5->new;
      my $print_cmd_key = sprintf( "%04X", $cmd_key );
      my $region_start  = $offset_table{$cmd_key}{offset};
      my $offset        = "";

      if( exists( $script_opcode{$print_cmd_key} ) ) {
         $print_cmd_key = sprintf( "%s (%s)",
                              $script_opcode{$print_cmd_key},
                              $print_cmd_key
                          );
      }

      if( exists( $file_offset{$region_start} ) ) {
         $offset = $region_start;
      } else {
         $tool{info_print}(
            sprintf( "Could not find %s in the document for command %s\n",
               $region_start,
               $print_cmd_key
            )
         );
         next;
      }

      $tool{standard_print}(
         sprintf( "%s for script function %s:\n",
            $region_start,
            $print_cmd_key
         )
      );

      my %script_hash = %{ getFullFunction( $offset ) };

      my %last_reg = (
         hi => "?",
         lo => "?",
         ra => "?",
         sp => "FFFFFFFF",
      );
      
      foreach my $regnum (1 ... 31) {
         $last_reg{"r$regnum"} = "?";
      }

      $last_reg{r1} = "800B0000";
      $last_reg{r2} = "$region_start";
      $last_reg{r3} = "script_index";
      $last_reg{r4} = "0000FFFF";
      $last_reg{r5} = "[800AFD1C]";

      foreach my $linenum ( sort( { $a <=> $b } keys( %script_hash ) ) ) {
         my %local_sub_calls;
         decodeScriptCommand( \%last_reg, \%local_sub_calls, $digest, $script_hash{$linenum} );
         foreach my $location (keys %local_sub_calls) {
            $sub_calls{$location} += $local_sub_calls{$location};
            $calls{$region_start}{$location} += 1;
         }
      }

      $tool{standard_print}( "\n\n" );
   }

   return \%sub_calls;
}

sub getFullFunction {
   my $offset = shift( @_ );
   my $linenum = 0;
   my %script_hash;

   while( $offset ne "" ) {
      my $line = $file_offset{$offset}{line};
      $offset  = $file_offset{$offset}{next};
   
      my @line_breaks = split( /\s+/, $line );
      my %script_cmd;
      my $numbreaks = scalar( @line_breaks );
   
      $script_cmd{offset} = shift( @line_breaks );
      $script_cmd{code}   = shift( @line_breaks );
      $script_cmd{extend} = "";
   
      my $temp_line = shift( @line_breaks );
   
      if( $temp_line eq "+" ) {
         $script_cmd{extend} = $temp_line;
         $script_cmd{name}   = shift( @line_breaks );
      } else {
         $script_cmd{name}   = $temp_line;
      }
   
      if( scalar( @line_breaks ) > 0 ) {
         my $input = 1;
         foreach my $cmd_input ( split( /,/, shift( @line_breaks ) ) ) {
            $script_cmd{inputs}{$input++} = $cmd_input;
         }
      }
   
      my $final = 0;
   
      if( $linenum > 0 and $script_hash{$linenum-1}{name} eq "ret" ) {
         $final = 1;
      }
   
      if( $script_cmd{extend} eq "+" ) {
         my $temp_ref = $script_hash{$linenum-1};
         if( $temp_ref->{name} =~ m/call/ or
             $temp_ref->{name} =~ m/ret/ ) {
            $script_hash{$linenum-1} = \%script_cmd;
            $script_hash{$linenum}   = $temp_ref;
         } else {
            $script_hash{$linenum} = \%script_cmd;
         }
      } elsif( $final == 0 ) {
         $script_hash{$linenum}   = \%script_cmd;
      } else {
         return \%script_hash;
      }
   
      $linenum++;
   }

   return \%script_hash;
}

sub decodeScriptCommand {
   my $last_reg   = shift( @_ );
   my $sub_calls  = shift( @_ );
   my $digest     = shift( @_ );
   my $script_cmd = shift( @_ );
   my $numargs    = 0;
   my $extended   = "";
   
   if( exists( $script_cmd->{inputs} ) ) {
      $numargs    = scalar( keys( %{ $script_cmd->{inputs} } ) );
   }

   if( $script_cmd->{extend} eq "+" ) {
      $extended = " (Command was an extention of the previous command)";
   }

   if( $script_cmd->{name} =~ m/(call)/ ) {
      my @argLocation = getArgument( $script_cmd->{inputs}->{1}, 0, $last_reg, \%offset_info );
      my $location    = $argLocation[1];
      my $opcode      = "";

      if( $extended ne "" ) {
         $extended = " (Command is executed before the following call command)";
      }

      if( exists( $offset_info{$location} ) ) {
         if( $offset_info{$location}{type} eq "opcode" ) {
            $opcode = $offset_info{$location}{type};
         }

         if( exists( $offset_info{$location}{inputs} ) ) {
            foreach my $known_input ( keys( %{ $offset_info{$location}{inputs} } ) ) {
               $tool{opcode_print}(
                  sprintf( "input(%s) = %s - %s\n",
                     $known_input,
                     $last_reg->{$known_input},
                     $offset_info{$location}{inputs}{$known_input}
                  ),
                  "printcalls"
               );
            }
         }

         $tool{opcode_print}(
            sprintf( "%s called %s (%s)%s\n",
               $script_cmd->{offset},
               $offset_info{$location}{name},
               $location,
               $extended
            ),
            "printcalls"
         );

         if( exists( $offset_info{$location}{outputs} ) ) {
            foreach my $known_output ( keys( %{ $offset_info{$location}{outputs} } ) ) {
               $last_reg->{$known_output} = $offset_info{$location}{outputs}{$known_output};
               $tool{opcode_print}(
                  sprintf( "output(%s) = %s - %s\n",
                     $known_output,
                     $last_reg->{$known_output},
                     $offset_info{$location}{outputs}{$known_output}
                  ),
                  "printcalls"
               );
            }
         }
      } else {
         $tool{opcode_print}(
            sprintf( "%s called %s%s\n",
               $script_cmd->{offset},
               $location,
               $extended
            ),
            "printcalls"
         );
      }
      # Don't add opcodes to the subfunction list.
      if( $opcode eq "" ) {
         $sub_calls->{$location}++;
      }
   } elsif( $script_cmd->{name} =~ m/(jmp)/ ) {
      my @argLocation = getArgument( $script_cmd->{inputs}->{1}, 2, $last_reg, \%offset_info );
      my $location    = $argLocation[1];

      if( exists( $offset_info{$location} ) ) {
         if( exists( $offset_info{$location}{inputs} ) ) {
            foreach my $known_input ( keys( %{ $offset_info{$location}{inputs} } ) ) {
               $tool{opcode_print}(
                  sprintf( "%s - %s - %s\n",
                     $known_input,
                     $last_reg->{$known_input},
                     $offset_info{$location}{inputs}{$known_input}
                  ),
                  "printjumps"
               );
            }
         }

         $tool{opcode_print}(
            sprintf( "%s Unconditional Jump %s (%s)%s\n",
               $script_cmd->{offset},
               $offset_info{$location}{name},
               $location,
               $extended
            ),
            "printjumps"
         );

         if( exists( $offset_info{$location}{outputs} ) ) {
            foreach my $known_output ( keys( %{ $offset_info{$location}{outputs} } ) ) {
               $last_reg->{$known_output} = $offset_info{$location}{outputs}{$known_output};
               $tool{opcode_print}(
                  sprintf( "%s - %s - %s\n",
                     $known_output,
                     $last_reg->{$known_output},
                     $offset_info{$location}{inputs}{$known_output}
                  ),
                  "printjumps"
               );
            }
         }
      } else {
         $tool{opcode_print}(
            sprintf( "%s Unconditional Jump %s%s\n",
               $script_cmd->{offset},
               $location,
               $extended
            ),
            "printjumps"
         );
      }
   } elsif( $script_cmd->{name} =~ m/(jnz|jz|jlez|jgtz|js|jns)/ ) {
      my @argValue    = getArgument( $script_cmd->{inputs}->{1}, 2, $last_reg, \%offset_info );
      my @argLocation = getArgument( $script_cmd->{inputs}->{2}, 2, $last_reg, \%offset_info );
      my $cmd_type    = "ERROR";
      
      if( $script_cmd->{name} =~ m/jlez/ ) {
         $cmd_type = "<=";
      } elsif( $script_cmd->{name} =~ m/jnz/ ) {
         $cmd_type = "!=";
      } elsif( $script_cmd->{name} =~ m/jz/ ) {
         $cmd_type = "==";
      } elsif( $script_cmd->{name} =~ m/jgtz/ ) {
         $cmd_type = ">";
      } elsif( $script_cmd->{name} =~ m/js/ ) {
         $cmd_type = "<";
      } elsif( $script_cmd->{name} =~ m/jns/ ) {
         $cmd_type = ">=";
      } else {
         $tool{fatal_print}(
           sprintf( "%s %s is unsupported",
              $script_cmd->{offset},
              $cmd_type
           )
        );
      }

      $tool{opcode_print}(
         sprintf( "%s Conditional Jump if( %s %s 0 ) goto %s%s\n",
            $script_cmd->{offset},
            $argValue[1],
            $cmd_type,
            $argLocation[1],
            $extended
         ),
         "printjumps"
      );
   } elsif( $script_cmd->{name} =~ m/(je|jne|jrel)/ ) {
      my @argLReturn  = getArgument( "00000000", 2, $last_reg, \%offset_info );
      my @argRReturn  = @argLReturn;
      my @argLocation = "00000000";
      my $cmd_type    = "ERROR";
      
      if( $numargs == 1 ) {
         @argLocation = getArgument( $script_cmd->{inputs}->{1}, 2, $last_reg, \%offset_info );
      } elsif( $numargs == 3 ) {
         @argLReturn  = getArgument( $script_cmd->{inputs}->{1}, 2, $last_reg, \%offset_info );
         @argRReturn  = getArgument( $script_cmd->{inputs}->{2}, 2, $last_reg, \%offset_info );
         @argLocation = getArgument( $script_cmd->{inputs}->{3}, 2, $last_reg, \%offset_info );
      } else {
         $tool{warning_print}(
            sprintf( "%s Command %s has unexpected number of inputs %d",
               $script_cmd->{offset},
               $script_cmd->{name},
               $numargs
            )
         );
      }

      if( $script_cmd->{name} =~ m/jne/ ) {
         $cmd_type = "!=";
      } elsif( $script_cmd->{name} =~ m/je/ ) {
         $cmd_type = "==";
      } elsif( $script_cmd->{name} =~ m/jrel/ ) {
         $cmd_type = "==";
      } else {
         $tool{fatal_print}(
            sprintf( "%s %s is unsupported",
               $script_cmd->{offset},
               $script_cmd->{name}
            )
         );
      }
      
      $tool{opcode_print}(
         sprintf( "%s Conditional Jump if( %s %s %s ) goto %s%s\n",
            $script_cmd->{offset},
            $argLReturn[1],
            $cmd_type,
            $argRReturn[1],
            $argLocation[1],
            $extended
         ),
         "printjumps"
      );
   } elsif( $script_cmd->{name} =~ m/(mov)/ ) {
      my @argLReturn = getArgument( $script_cmd->{inputs}->{1}, 1, $last_reg, \%offset_info );
      my @argRReturn = getArgument( $script_cmd->{inputs}->{2}, 2, $last_reg, \%offset_info );
      my $printtype  = "";

      if( $argLReturn[0] =~ m/reg/ ) {
         my $cmd_reg  = $argLReturn[1];
         my $cmd_desc = $argLReturn[2];

         $last_reg->{$cmd_reg} = $argRReturn[1];
      }

      if( $argLReturn[0] =~ m/ptr/ or
          $argRReturn[1] =~ m/ptr/ ) {
         $printtype = "printmemmoves";
      } elsif( $argRReturn[1] =~ m/reg/ ) {
         $printtype = "printregmoves";
      } else {
         $printtype = "printmoves";
      }
      
      $tool{opcode_print}(
         sprintf( "%s %s = %s = %s%s\n",
            $script_cmd->{offset},
            $argLReturn[1],
            $script_cmd->{inputs}->{2},
            $argRReturn[1],
            $extended
         ),
         $printtype
      );
   } elsif( $script_cmd->{name} =~ m/(not|neg)/ ) {
      my $cmd_lvalue = $script_cmd->{inputs}->{1};
      my $cmd_rvalue = $cmd_lvalue;
      my $cmd_type   = "ERROR";

      if( $numargs == 2 ) {
         $cmd_rvalue = $script_cmd->{inputs}->{2};
      }
      
      if( $script_cmd->{name} =~ m/not/ ) {
         $cmd_rvalue = "0-$cmd_rvalue-1";
         $cmd_type   = "inverted";
      } elsif( $script_cmd->{name} =~ m/neg/ ) {
         $cmd_rvalue = "0-$cmd_rvalue";
         $cmd_type   = "negated";
      } else {
         $tool{fatal_print}(
            sprintf( "%s %s is unsupported",
               $script_cmd->{offset},
               $script_cmd->{name}
            )
         );
      }
      
      my @argLReturn = getArgument( $cmd_lvalue, 1, $last_reg, \%offset_info );
      my @argRReturn = getArgument( $cmd_rvalue, 2, $last_reg, \%offset_info );
      
      $last_reg->{$argLReturn[1]} = $argRReturn[1]; 

      $tool{opcode_print}(
         sprintf( "%s %s %s = %s%s\n",
            $script_cmd->{offset},
            $cmd_type,
            $argLReturn[1],
            $argRReturn[1],
            $extended
         ),
         "printarithmetic"
      );
   } elsif( $script_cmd->{name} =~ m/(lwl|lwr)/ ) {
      my $cmd_type     = $script_cmd->{name};
      my $cmd_lvalue   = $script_cmd->{inputs}->{1};
      my $cmd_rvalue   = $script_cmd->{inputs}->{2};

      if( $cmd_type =~ m/lwl/ ) {
         # Preserve the lower bits
         $cmd_rvalue = "$cmd_lvalue&0000FFFF|$cmd_rvalue<<00000010";
      } elsif( $cmd_type =~ m/lwr/ ) {
         # Preserve the upper bits
         $cmd_rvalue = "$cmd_lvalue&FFFF0000|$cmd_rvalue";
      }

      my @argLReturn = getArgument( $cmd_lvalue, 1, $last_reg, \%offset_info );
      my @argRReturn = getArgument( $cmd_rvalue, 2, $last_reg, \%offset_info );
      
      $last_reg->{$argLReturn[1]} = $argRReturn[1]; 

      $tool{opcode_print}(
         sprintf( "%s %s = %s = %s%s\n",
            $script_cmd->{offset},
            $argLReturn[1],
            $cmd_rvalue,
            $argRReturn[1],
            $extended
         ),
         "printmemmoves"
      );
   } elsif( $script_cmd->{name} =~ m/(swl|swr)/ ) {
      my $cmd_type     = $script_cmd->{name};
      my $cmd_lvalue   = $script_cmd->{inputs}->{1};
      my $cmd_rvalue   = $script_cmd->{inputs}->{2};

      if( $cmd_type =~ m/swl/ ) {
         # Only get the upper 16-bits
         $cmd_rvalue = "$cmd_rvalue>>00000010";
      } elsif( $cmd_type =~ m/swr/ ) {
         # Only get the lower 16-bits
         $cmd_rvalue = "$cmd_rvalue&0000FFFF";
      }

      my @argLReturn = getArgument( $cmd_lvalue, 1, $last_reg, \%offset_info );
      my @argRReturn = getArgument( $cmd_rvalue, 2, $last_reg, \%offset_info );
      
      $tool{opcode_print}(
         sprintf( "%s %s = %s = %s%s\n",
            $script_cmd->{offset},
            $argLReturn[1],
            $cmd_rvalue,
            $argRReturn[1],
            $extended
         ),
         "printmemmoves"
      );
   } elsif( $script_cmd->{name} =~ m/(setz|setnz)/ ) {
      my $cmd_type     = $script_cmd->{name};
      my $cmd_lvalue   = $script_cmd->{inputs}->{1};
      my $cmd_rvalue1  = "00000000";
      my $cmd_rvalue2  = "00000000";
      my $operation    = "";

      if( $cmd_type =~ m/setz/ ) {
         $operation   = "<";
         $cmd_rvalue1 = $cmd_lvalue;
         $cmd_rvalue2 = "00000001";
      } elsif( $cmd_type =~ m/setnz/ ) {
         $operation   = "<";
         $cmd_rvalue1 = "00000000";
         $cmd_rvalue2 = $cmd_lvalue;
      } else {
         $tool{fatal_print}(
            sprintf( "%s %s is unsupported",
               $script_cmd->{offset},
               $script_cmd->{name}
            )
         );
      }

      my @argLReturn = getArgument( $cmd_lvalue,                          1, $last_reg, \%offset_info );
      my @argRReturn = getArgument( $cmd_rvalue1.$operation.$cmd_rvalue2, 2, $last_reg, \%offset_info );
      
      $last_reg->{$argLReturn[1]} = $argRReturn[1]; 

      $tool{opcode_print}(
         sprintf( "%s %s = %s %s %s = %s%s\n",
            $script_cmd->{offset},
            $argLReturn[1],
            $cmd_rvalue1,
            $operation,
            $cmd_rvalue2,
            $argRReturn[1],
            $extended
         ),
         "printarithmetic"
      );
   } elsif( $script_cmd->{name} =~ m/(mul|div)/ ) {
      # Multiply and Divide function is unique as it saves to hi and lo rather than the lvalue
      my $cmd_type     = $script_cmd->{name};
      my $cmd_rvalue1  = $script_cmd->{inputs}->{1};
      my $cmd_rvalue2  = $script_cmd->{inputs}->{2};
      my $cmd_op       = "";

      if( $cmd_type =~ m/mul/i ) {
         # mul r1,r2 = {hi,lo}
         $cmd_op = "*";
         my @argRReturn = getArgument( $cmd_rvalue1.'*'.$cmd_rvalue2, 2, $last_reg, \%offset_info );

         if( $argRReturn[0] eq "constconst" ) {
            $last_reg->{lo} = substr( $argRReturn[1], 0, 8 );
            $last_reg->{hi} = substr( $argRReturn[1], 8, 8 );
         } else {
            $last_reg->{lo} = $argRReturn[1];
            $last_reg->{hi} = $argRReturn[1];
         }
      } elsif( $cmd_type =~ m/div/i ) {
         # div r1,r2 => lo = r1/r2, hi = r1 mod r2
         $cmd_op = "/";
         my @argDiv = getArgument( $cmd_rvalue1.'/'.$cmd_rvalue2, 2, $last_reg, \%offset_info );
         my @argMod = getArgument( $cmd_rvalue1.'%'.$cmd_rvalue2, 2, $last_reg, \%offset_info );

         $last_reg->{lo} = $argDiv[1];
         $last_reg->{hi} = $argMod[1];
      } else {
         $tool{fatal_print}(
            sprintf( "%s %s is unsupported",
               $script_cmd->{offset},
               $script_cmd->{name}
            )
         );
      }

      $tool{opcode_print}(
         sprintf( "%s %s %s %s => hi = %s, lo = %s%s\n",
            $script_cmd->{offset},
            $cmd_rvalue1,
            $cmd_op,
            $cmd_rvalue2,
            $last_reg->{hi},
            $last_reg->{lo},
            $extended
         ),
         "printarithmetic"
      );
   } elsif( $script_cmd->{name} =~ m/(add|sub|or|and|sal|shl|sar|shr|setlt|setgt|setb)/ ) {
      my $cmd_type     = $script_cmd->{name};
      my $cmd_lvalue   = $script_cmd->{inputs}->{1};
      my $cmd_rvalue1  = $script_cmd->{inputs}->{1};
      my $cmd_rvalue2  = $script_cmd->{inputs}->{2};
      my $operation    = "";

      if( $numargs == 3 ) {
         $cmd_rvalue1 = $cmd_rvalue2;
         $cmd_rvalue2 = $script_cmd->{inputs}->{3};
      }
      
      if( $cmd_type =~ m/add/i ) {
         $operation = "+";
      } elsif( $cmd_type =~ m/sub/i ) {
         $operation = "-";
      } elsif( $cmd_type =~ m/or/i ) {
         $operation = "|";
      } elsif( $cmd_type =~ m/and/i ) {
         $operation = "&";
      } elsif( $cmd_type =~ m/sal|shl/i ) {
         $operation = "<<";
      } elsif( $cmd_type =~ m/sar|shr/i ) {
         $operation = ">>";
      } elsif( $cmd_type =~ m/setlt|setb/i ) {
         $operation = "<";
      } elsif( $cmd_type =~ m/setgt/i ) {
         $operation = ">";
      } else {
         $tool{fatal_print}(
            sprintf( "%s %s is unsupported",
               $script_cmd->{offset},
               $script_cmd->{name}
            )
         );
      }
      
      my @argLReturn = getArgument( $cmd_lvalue,                          1, $last_reg, \%offset_info );
      my @argRReturn = getArgument( $cmd_rvalue1.$operation.$cmd_rvalue2, 2, $last_reg, \%offset_info );
      
      $last_reg->{$argLReturn[1]} = $argRReturn[1]; 

      $tool{opcode_print}(
         sprintf( "%s %s = %s %s %s = %s%s\n",
            $script_cmd->{offset},
            $argLReturn[1],
            $cmd_rvalue1,
            $operation,
            $cmd_rvalue2,
            $argRReturn[1],
            $extended
         ),
         "printarithmetic"
      );
   } elsif( $script_cmd->{name} =~ m/(ret|nop)/ ) {
      if( $1 eq "ret" and $extended ne "" ) {
         $extended = " (Command is an extention of the following return)";
      }

      $tool{opcode_print}(
         sprintf( "%s %s\n",
            $script_cmd->{offset},
            $script_cmd->{name}
         ),
         "printalways"
      );
      
      if( $1 eq "ret" ) {
         return 1;
      }
   } elsif( $script_cmd->{name} =~ m/(push|pop)/ ) {
      $tool{opcode_print}(
         sprintf( "%s %s %s%s\n",
            $script_cmd->{offset},
            $script_cmd->{name},
            $script_cmd->{inputs}->{1},
            $extended
         ),
         "printmemmoves"
      );
   } elsif( $script_cmd->{name} =~ m/(break)/ ) {
      $tool{opcode_print}(
         sprintf( "%s debugger break point with value %s%s\n",
            $script_cmd->{offset},
            $script_cmd->{inputs}->{1},
            $extended
         ),
         "printalways"
      );
   } else {
      my @inputlist = ();
      if( exists( $script_cmd->{inputs} ) ) {
         foreach my $input ( sort( { $a <=> $b } keys( %{ $script_cmd->{inputs} } ) ) ) {
            push( @inputlist, $script_cmd->{inputs}->{$input} );
         }
      }
      $tool{warning_print}(
         sprintf( "%s Unknown Command %s %s%s",
            $script_cmd->{offset},
            $script_cmd->{name},
            join( ', ', @inputlist ),
            $extended
         )
      );
   }

   return 0;
}

sub getArgument {
   my $argument        = shift( @_ );
   my $position        = shift( @_ );
   my $last_reg_ref    = shift( @_ );
   my $offset_info_ref = shift( @_ );
   my $type            = "";
   my $return_value    = "";
   my $description     = "";

   my %argument_list;

   if( $argument =~ m/^\[(.*)\]$/ ) {
      $type        = "ptr";
      $description = "Pointer type value";
   }

   if( $argument =~ m/^((?:(?:r|k|t|a|s)\d+)|sp|gp|fp|ra|hi|lo|at|pc)$/ ) {
      $type        = "reg" . $type;
      $description = "Register type for register $1";
   } else {
      $type        = "const" . $type;
      $description = "Constant type value";
   }
   
   my $final_value_ref;
   my %final_value;
   
   if( $position > 1 or $type ne "reg" ) {
      rpn_buildargs( 0, $argument, \%argument_list, $last_reg_ref );
      $final_value_ref = rpn_calculator( \%argument_list, $last_reg_ref, $offset_info_ref );
   } else {
      my %final_value;
      
      $final_value{value}       = $argument;
      $final_value{type}        = $type;
      $final_value{description} = "reg";
      $final_value_ref          = \%final_value;
   }
   
   my $strip_final_value = $final_value_ref->{value};
   $strip_final_value =~ s/\(|\)//g;
   
   if( $position <= 1 and $type eq "reg" ) {
      $return_value = $final_value_ref->{value};
      $description  = $final_value_ref->{description};
      $type         = $type . $final_value_ref->{type};
   } elsif( $position > 0 and exists( $offset_info_ref->{$strip_final_value} ) ) {
      $return_value = $offset_info_ref->{$strip_final_value}->{name};
      $description  = $offset_info_ref->{$strip_final_value}->{description};
      $type         = $type . $final_value_ref->{type};
   } else {
      $return_value = $final_value_ref->{value};
      $description  = "Unknown " .
                      $type . " " .
                      $final_value_ref->{type} . " " .
                      $final_value_ref->{value} . " " .
                      $final_value_ref->{description};
      $type         = $type . $final_value_ref->{type};
   }

   return ($type, $return_value, $description);
}

sub rpn_buildargs {
   my $arg_index    = shift( @_ );
   my $argument     = shift( @_ );
   my $arg_list_ref = shift( @_ );
   my $last_reg_ref = shift( @_ );

   my $group7 = "\\[|\\]";
   my $group6 = "\\(|\\)";
   my $group5 = "\\*|\\/|\\%";
   my $group4 = "\\+|\\-";
   my $group3 = "\\<\\<|\\>\\>";
   my $group2 = "\\<|\\<\\=|\\>|\\>\\=";
   my $group1 = "\\&|\\|";
   my $group = join( "|", $group7, $group6, $group5, $group4, $group3, $group2, $group1 );
   
   my @arguments = split( /($group)/, $argument );
   
   $tool{debug_print}( "rpn_buildargs: $argument" );

   foreach my $arg ( @arguments ) {
      if( $arg =~ m/^((?:(?:r|k|t|a|s)\d+)|sp|gp|fp|ra|hi|lo|at|pc)$/ ) {
         $arg_index = rpn_buildargs( $arg_index, $last_reg_ref->{$arg}, $arg_list_ref, $last_reg_ref );
      } else {
         if( $arg =~ m/^($group7)$/ ) {
            $arg_list_ref->{$arg_index}->{value}       = $arg;
            $arg_list_ref->{$arg_index}->{type}        = "op6";
            $arg_list_ref->{$arg_index}->{description} = "Square brace representing address pointer";
            $arg_index = $arg_index + 1;
         } elsif( $arg =~ m/^($group6)$/ ) {
            $arg_list_ref->{$arg_index}->{value}       = $arg;
            $arg_list_ref->{$arg_index}->{type}        = "op6";
            $arg_list_ref->{$arg_index}->{description} = "Parenthesis is meant to be used for groups";
            $arg_index = $arg_index + 1;
         } elsif( $arg =~ m/^($group5)$/ ) {
            $arg_list_ref->{$arg_index}->{value}       = $arg;
            $arg_list_ref->{$arg_index}->{type}        = "op5";
            $arg_list_ref->{$arg_index}->{description} = "Multiplication group";
            $arg_index = $arg_index + 1;
         } elsif( $arg =~ m/^($group4)$/ ) {
            my $operation = $1;
            # Assume that the - is used to indicated negative if it was
            # the first argument or the previous argument was an opcode
            if( $arg_index == 0 or
               ( $arg_list_ref->{$arg_index-1}->{type} =~ m/op/ and
                  $arg_list_ref->{$arg_index-1}->{value} =~ m/[^\)\]]/ ) ) {
               $arg_index = rpn_buildargs( $arg_index, "0", $arg_list_ref, $last_reg_ref );
            }
            $arg_list_ref->{$arg_index}->{value}       = $arg;
            $arg_list_ref->{$arg_index}->{type}        = "op4";
            $arg_list_ref->{$arg_index}->{description} = "Addition group";
            $arg_index = $arg_index + 1;
         } elsif( $arg =~ m/^($group3)$/ ) {
            $arg_list_ref->{$arg_index}->{value}       = $arg;
            $arg_list_ref->{$arg_index}->{type}        = "op3";
            $arg_list_ref->{$arg_index}->{description} = "Bit level operations";
            $arg_index = $arg_index + 1;
         } elsif( $arg =~ m/^($group2)$/ ) {
            $arg_list_ref->{$arg_index}->{value}       = $arg;
            $arg_list_ref->{$arg_index}->{type}        = "op2";
            $arg_list_ref->{$arg_index}->{description} = "Comparator operations";
            $arg_index = $arg_index + 1;
         } elsif( $arg =~ m/^($group1)$/ ) {
            $arg_list_ref->{$arg_index}->{value}       = $arg;
            $arg_list_ref->{$arg_index}->{type}        = "op1";
            $arg_list_ref->{$arg_index}->{description} = "Bit level operations";
            $arg_index = $arg_index + 1;
         } elsif( $arg =~ m/^([0-9A-F]+)h?$/ ) {
            $arg_list_ref->{$arg_index}->{value}       = sprintf( "%08X", hex($1) );
            $arg_list_ref->{$arg_index}->{type}        = "const";
            $arg_list_ref->{$arg_index}->{description} = "Constant Value " . $arg_list_ref->{$arg_index}->{value};
            $arg_index = $arg_index + 1;
         } elsif( $arg ne "" ) {
            $arg_list_ref->{$arg_index}->{value}       = $arg;
            $arg_list_ref->{$arg_index}->{type}        = "string";
            $arg_list_ref->{$arg_index}->{description} = "String value " . $arg_list_ref->{$arg_index}->{value};
            $arg_index = $arg_index + 1;
         }
         
         if( $arg ne "" ) {
            $tool{debug_print}( "rpn_buildargs " . ($arg_index-1) . ": " . $arg_list_ref->{$arg_index-1}->{value} . "\n" );
         }
      }
   }
      
   return $arg_index;
}

sub rpn_solver {
   my $opcodes_ref     = shift( @_ );
   my $args_ref        = shift( @_ );
   my $offset_info_ref = shift( @_ );
   my $rvalue_ref      = pop_arguments( "args", $args_ref );
   my $lvalue_ref      = pop_arguments( "args", $args_ref );
   my $opcode_ref      = pop_arguments( "opcodes", $opcodes_ref );
   my $opcode_level    = 0;
   my %final_argument;
   
   if( !defined( $opcode_ref ) ) {
      $tool{fatal_print}( "Opcode pulled from opcode stack is missing" );
   } elsif( $opcode_ref->{type} =~ m/op(\d)/ ) {
      $opcode_level = $1;
   }
   
   if( !defined( $rvalue_ref ) ) {
      $tool{standard_print}( "rpn_solver: Opcode value was $opcode_ref->{value}\n" );
      $tool{fatal_print}( "RValue pulled from argument stack is missing" );
   } elsif( !defined( $lvalue_ref ) ) {
      $tool{standard_print}( "rpn_solver: Opcode value was $opcode_ref->{value}\n" );
      $tool{standard_print}( "rpn_solver: Right value was $rvalue_ref->{value}\n" );
      $tool{fatal_print}( "LValue pulled from argument stack is missing" );
   }
   my $value = $lvalue_ref->{value} . $opcode_ref->{value} . $rvalue_ref->{value};
   
   #if( $opcode_ref->{value} =~ m/(\+|\-|\*|\/|\||\&)/ ) {
   #   my $swap = 0;
   #   
   #   if( $rvalue_ref->{type} eq "string" ) {
   #      if( $lvalue_ref->{type} eq "string" ) {
   #         if( $rvalue_ref->{value} lt $lvalue_ref->{value} ) {
   #            $swap = 1;
   #         }
   #      } else {
   #         $swap = 1;
   #      }
   #   } else {
   #      if( $lvalue_ref->{type} eq "const" ) {
   #         if( hex($rvalue_ref->{value}) < hex($lvalue_ref->{value}) ) {
   #            $swap = 1;
   #         }
   #      }
   #   }
   #   
   #   if( $swap ) {
   #      my $temp_ref = $rvalue_ref;
   #      $rvalue_ref = $lvalue_ref;
   #      $lvalue_ref = $temp_ref;
   #   }
   #}
   
   $value = $lvalue_ref->{value} . $opcode_ref->{value} . $rvalue_ref->{value};
   
   $tool{debug_print}( "rpn_solver: $value" );
   
   if( $lvalue_ref->{type} ne "string" and $rvalue_ref->{type} ne "string" ) {
      $value = hex($lvalue_ref->{value}) . $opcode_ref->{value} . hex($rvalue_ref->{value});
      $tool{debug_print}( "rpn_solver: $value" );
      eval "\$value = $value"; warn $@ if $@;
      if( $opcode_ref->{value} eq "*" ) {
         $value = sprintf( "%016X", $value );
      } else {
         $value = sprintf( "%08X", $value );
      }
      $value =~ s/^(0+)([0-9A-F]+)/$2/;
      $final_argument{type}        = "const";
      $final_argument{description} = "unknown constant value";
   } else {
      if( $lvalue_ref->{type} ne "string" ) {
         $value = $lvalue_ref->{value};
         $value =~ s/^(0+)([0-9A-F]+)/$2/;
         $lvalue_ref->{value} = $value;
      }
      if( $rvalue_ref->{type} ne "string" ) {
         $value = $rvalue_ref->{value};
         $value =~ s/^(0+)([0-9A-F]+)/$2/;
         $rvalue_ref->{value} = $value;
      }

      $final_argument{type}        = "string";
      $final_argument{description} = "unknown string value";
      $value                       = $lvalue_ref->{value} . $opcode_ref->{value} . $rvalue_ref->{value};
   }
   
   my $value_strip = $value;
   $value_strip =~ s/\(|\)//g;
   
   if( exists( $offset_info_ref->{$value_strip} ) ) {
      $final_argument{type}        = "string";
      $final_argument{description} = $offset_info_ref->{$value_strip}->{description};
      $final_argument{value}       = $offset_info_ref->{$value_strip}->{name};
   } else {
      $final_argument{value} = $value;
   }

   push_arguments( "args", $args_ref, \%final_argument );
}

sub push_arguments {
   my $array_name = shift( @_ );
   my $array_ref  = shift( @_ );
   my $value_ref  = shift( @_ );
   
   push( @$array_ref, $value_ref );
   
   $tool{debug_print}( "Pushed into the array " . $array_name .
          ":\n" . Dumper( $value_ref ) );
   
   return $value_ref;
}

sub pop_arguments {
   my $array_name = shift( @_ );
   my $array_ref  = shift( @_ );
   my $value_ref;

   if( @$array_ref == 0 ) {
      $tool{fatal_print}("Attempted to underflow $array_name");
   }
   
   $value_ref = pop( @$array_ref );
   
   $tool{debug_print}( "Popped from the array " . $array_name .
          ":\n" . Dumper( $value_ref ) );

   return $value_ref;
}

sub rpn_calculator {
   my $arg_list_ref      = shift( @_ );
   my $last_reg_ref      = shift( @_ );
   my $offset_info_ref   = shift( @_ );
   my $cur_opcode_level  = 0;
   my $prev_opcode_level = 0;
   my $prev_open_brace   = "";
   my $prev_close_brace  = "";
   my @prev_braces       = ();
   my @prev_levels       = ();
   my @opcodes           = ();
   my @args              = ();
   my $arg_count         = 0;

   foreach my $arg ( sort( { $a <=> $b } keys( %$arg_list_ref ) ) ) {
      if( $arg_list_ref->{$arg}->{type} =~ m/^op(\d)$/ ) {
         $cur_opcode_level = $1;

         # Uses a brace
         if( $cur_opcode_level == 6 ) {
            if( $arg_list_ref->{$arg}->{value} =~ m/^(\(|\[)$/ ) {
               my $cur_open_brace = $1;
               if( $prev_open_brace ne "" ) {
                  push_arguments( "prev_braces", \@prev_braces, $prev_open_brace );
               }
               $prev_open_brace = $cur_open_brace;
               push_arguments( "prev_levels", \@prev_levels, $prev_opcode_level );
               $prev_opcode_level = 0;
               push_arguments("opcodes", \@opcodes, $arg_list_ref->{$arg} );
            } elsif( $arg_list_ref->{$arg}->{value} =~ m/^(\)|\])$/ ) {
               # IF the closing brace then there are some steps required
               my $cur_close_brace = $1;
               
               # Get the expected open brace based on the currently visible
               # close brace.
               my $exp_open_brace = "";
               if( $cur_close_brace eq ")" ) {
                  $exp_open_brace = "(";
               } elsif( $cur_close_brace eq "]" ) {
                  $exp_open_brace = "[";
               } else {
                  $tool{fatal_print}( "Unknown Close brace ($cur_close_brace)" );
               }
               
               # If there was no previous open brace we have problems.
               if( $prev_open_brace eq "" ) {
                  $tool{fatal_print}( "Close brace ($cur_close_brace) found with no open braces" );
               } elsif( $prev_open_brace ne $exp_open_brace ) {
                  # If there was a previously open brace but doesn't match then we have
                  # a bad command string being injected.
                  $tool{fatal_print}( "Previous open brace ($prev_open_brace) does not match with current close brace ($cur_close_brace)" );
               } else {
                  # So long as the first two check passed pick out the opcode
                  # at the top...
                  my $prev_opcode_ref = pop_arguments( "opcodes", \@opcodes );
                  while( $prev_opcode_ref->{value} ne $exp_open_brace ) {
                     # loop until the expected open brace is found.
                     # If it was not found push the opcode back in and solve
                     # for that opcode.
                     push_arguments( "opcodes", \@opcodes, $prev_opcode_ref );
                     rpn_solver( \@opcodes, \@args, $offset_info_ref );
                     # Try testing the opcode at the top of the stack again.
                     $prev_opcode_ref = pop_arguments( "opcodes", \@opcodes );
                     
                     # If there was more to the command string prior to the
                     # brace we need to recover what that level was. This
                     # should always be true as we would have pushed in zero
                     # reguardless.
                     $prev_opcode_level = pop_arguments( "prev_levels", \@prev_levels );
                  }
                  $prev_opcode_level = pop_arguments( "prev_levels", \@prev_levels );
                  # If the previous expected open brace was for a square bracket
                  # Then it is actually a memory pointer, we want to preserve
                  # the square brackets so put them back and make the argument
                  # a string type.
                  
                  my $arg_ref = pop_arguments( "args", \@args );
                  if( $prev_opcode_ref->{value} eq "[" or
                      ( $prev_opcode_ref->{value} eq "(" and
                        $arg_ref->{type} eq "string" ) ) {
                     $arg_ref->{value} = $exp_open_brace . $arg_ref->{value} . $cur_close_brace;
                     $arg_ref->{type}  = "string";
                     
                     $tool{debug_print}( "rpn_calculator: $arg_ref->{value}" );
                  }
                  push_arguments( "args", \@args, $arg_ref );
                  
                  # If there was another brace outside the one we executed
                  # on we need to go recover the brace that was used before.
                  if( @prev_braces > 0 ) {
                     $prev_open_brace = pop_arguments( "prev_braces", \@prev_braces );
                  } else {
                     $prev_open_brace = "";
                  }
               }
            }
         } else {
# If the current opcode is lower than the last one:
#   process the last opcode first.
# If the current opcode is equal to the last one:
#   process the last opcode first.
# If the current opcode is greater than the last one:
#   The current may be a higher level, but assume next one can be higher.
            while( $cur_opcode_level <= $prev_opcode_level ) {
               rpn_solver( \@opcodes, \@args, $offset_info_ref );
               # Get the opcode level of the opcode before this one.
               $prev_opcode_level = pop_arguments( "prev_levels", \@prev_levels  );
               $tool{debug_print}( "rpn: $prev_opcode_level" );
            }
            push_arguments( "prev_levels", \@prev_levels, $prev_opcode_level );
            $prev_opcode_level = $cur_opcode_level;
            push_arguments( "opcodes", \@opcodes, $arg_list_ref->{$arg} );
         }
      } else {
         push_arguments( "args", \@args, $arg_list_ref->{$arg} );
         $arg_count = $arg_count+1;
      }
   }

# Exhaustively complete the remaining opcodes
   while( @opcodes > 0 ) {
      rpn_solver( \@opcodes, \@args, $offset_info_ref );
      $prev_opcode_level = pop_arguments( "prev_levels", \@prev_levels );
   }
   
   my $arg_ref = pop_arguments( "args", \@args );
   my $arg_cnt = () = $arg_ref->{value} =~ m/\(|\)|\*|\/|\+|\-|\<\<|\>\>|\&|\||\%/g;
   # Not the most sophisticated thing but if there were more than
   # 1 arguments in the original list then put in a braces to prevent
   # loss of data.
   if( $arg_ref->{type} eq "string" and
       $arg_cnt > 0 ) {
      $arg_ref->{value} = "($arg_ref->{value})";
   }

# Return back with the final results
   return $arg_ref;
}
