using System;

public class Triangle : IComparable<Triangle> {
	public int shader;
	public Vertex[] vertex;
	
	public int CompareTo(Triangle rhs) {
		if (shader != rhs.shader) {
			return shader.CompareTo(rhs.shader);
		}
		for(int j=0; j<vertex.Length; j++) {
			int compare = vertex[j].CompareTo(rhs.vertex[j]);
			if (compare != 0) {
				return compare;
			}
		}
		return 0;
	}
};
