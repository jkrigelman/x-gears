using System;

public class XGShader : IEquatable<XGShader> {
	public uint status;
	public uint clut;
	public bool abe;
	public bool tme;
	
	public XGShader(uint status, uint clut, bool abe, bool tme) {
		this.status = status;
		this.clut = clut;
		this.abe = abe;
		this.tme = tme;
	}
	
	public bool Equals(XGShader rhs) {
		if (rhs == null) {
			return false;
		}
		if (status != rhs.status) {
			return false;
		}
		if (clut != rhs.clut) {
			return false;
		}
		if (abe != rhs.abe) {
			return false;
		}
		if (tme != rhs.tme) {
			return false;
		}
		return true;
	}
};