using System;

public class Vertex : IComparable<Vertex> {
	public int u, v;
	public uint r, g, b, a;
	public int x,y,z;
	public int nx,ny,nz;
	public int CompareTo(Vertex rhs) {
		if (u != rhs.u) {
			return u.CompareTo(rhs.u);
		}
		if (v != rhs.v) {
			return v.CompareTo(rhs.v);
		}
		if (r != rhs.r) {
			return r.CompareTo(rhs.r);
		}
		if (g != rhs.g) {
			return g.CompareTo(rhs.g);
		}
		if (b != rhs.b) {
			return b.CompareTo(rhs.b);
		}
		if (a != rhs.a) {
			return a.CompareTo(rhs.a);
		}
		if (x != rhs.x) {
			return x.CompareTo(rhs.x);
		}
		if (y != rhs.y) {
			return y.CompareTo(rhs.y);
		}
		if (z != rhs.z) {
			return z.CompareTo(rhs.z);
		}
		if (nx != rhs.nx) {
			return nx.CompareTo(rhs.nx);
		}
		if (ny != rhs.ny) {
			return ny.CompareTo(rhs.ny);
		}
		return nz.CompareTo(rhs.nz);
	}
};